features:
  primary:
    - name: "License Approval Policies"
      available_in: [ultimate]
      gitlab_com: true
      documentation_link: 'https://docs.gitlab.com/ee/user/compliance/license_approval_policies.html'
      video: https://www.youtube-nocookie.com/embed/34qBQ9t8qO8
      reporter: sam.white
      stage: govern
      categories:
        - 'Security Policy Management'
      issue_url: 'https://gitlab.com/groups/gitlab-org/-/epics/8092'
      description: |
        Users are now able to manage their License Approval Policies by creating a Security & Compliance Scan Result Policy.
        
        GitLab now supports flexible License approval policies as the replacement for the deprecated License-Check feature. License approval policies are similar to License-Check in that both can require approvals for MRs based on licenses found in project dependencies. However, license approval policies improve the previous experience in several ways:

        - Users can choose who is allowed to edit license approval policies. An independent legal or compliance team can therefore manage policies in a way that prevents development project maintainers from modifying the rules.
        - Multiple policy rules can be created and chained together to allow for complex criteria to determine when approval is required.
        - A two-step approval process can be enforced for any desired changes to license approval policies.
        - A single set of license policies can be applied to multiple development projects, or can be applied at the group or subgroup level, to allow for ease in maintaining a single, centralized ruleset.
        - In addition to specifying which specific licenses are denied, policies can also be used to require approval for any license that is not specifically allowed.
        
        License approval policies can be used alongside the existing License-Check feature, as the two policies are additive and don't conflict. However, we encourage users to migrate their License-Check rules over to license approval policies. License-Check rules are now [deprecated](https://docs.gitlab.com/ee/update/deprecations.html#license-check-and-the-policies-tab-on-the-license-compliance-page), and are scheduled for removal in GitLab 16.0. To get started, verify that the `license_scanning_policies` feature flag is enabled for your instance and then navigate to **Security & Compliance > Policies**, create a new Scan Result type policy, and select "License scanning" for your policy rule.
